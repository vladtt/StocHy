/*
 * Stochy main file
 *
 *  Created on: 14 Nov 2017
 *      Author: nathalie
 */

#include "taskExec.h"
#include "time.h"
#include <iostream>
#include <nlopt.hpp>

int main(int argc, char **argv) {

  std::cout << " _______  _______  _______  _______  __   __  __   __ "
            << std::endl;
  std::cout << "|       ||       ||       ||       ||  | |  ||  | |  |"
            << std::endl;
  std::cout << "|  _____||_     _||   _   ||       ||  |_|  ||  |_|  |"
            << std::endl;
  std::cout << "| |_____   |   |  |  | |  ||       ||       ||       |"
            << std::endl;
  std::cout << "|_____  |  |   |  |  |_|  ||      _||       ||_     _|"
            << std::endl;
  std::cout << " _____| |  |   |  |       ||     |_ |   _   |  |   |  "
            << std::endl;
  std::cout << "|_______|  |___|  |_______||_______||__| |__|  |___| "
            << std::endl;
  std::cout << std::endl;
  std::cout << " Welcome!  Copyright (C) 2018  natchi92 " << std::endl;
  std::cout << std::endl;

  // ------------------------- Case study 1 - Verification
  // -----------------------------------
  std::cout << "------------ Performing Case Study 1 : Formal Verification "
               "of CO2 model -----------"
            << std::endl;
  std::cout << std::endl;

  // Get model from file
  //shs_t<arma::mat, int> cs1SHS("../CS1.mat");

  // To specify model manually 
  // comment the previous line of code
  // i.e. shs_t<arma::mat,int> cs1SHS("../CS1.mat'")
  // and then uncomment the following lines of code
  // (i.e. lines 51-55) which allow you to manually 
  // the model dynamics using the kernel Tx = \N(Aqix, Gqi) where
  // \N is the normal distribution with mean and variance
  // ---------------------------------------------
     arma::mat Aq0 = {{0.935, 0},{0, 0.9157}};
     arma::mat Gq0 = {{1.7820, 0},{0, 0.5110}};
     ssmodels_t model(Aq0,Gq0);
     arma::vec Bq0 = {{0.5},{1}};
     model.B = Bq0;
     std::vector<ssmodels_t> models = {model};
     shs_t<arma::mat,int> cs1SHS(models);
  // --------------------------------------------
  
  // Specify verification task
  // Starting with FAUST^2 
  std::cout << " Formal verification via FAUST^2" << std::endl;
  std::cout << std::endl;
  // Define max error
  // Maximal error to obtain specific state space sizes
  // --------------------------------------------------------
  // State space size| 576 | 1089 | 2304  |  3481 | 4225 
  // ________________________________________________________
  // Max Error       | 2.5 | 1.8  | 1.25  | 1.02  | 0.90
  // --------------------------------------------------------
  
  // Define max error for 576 states
  double eps =2.5;

  // Define safe set
  arma::mat Safe = {{18, 24}, {18, 24}};

  arma::mat Target = {{21, 22}, {21, 22}};

  arma::mat Input = {{0, 1}};

  // Define grid type
  // (1 = uniform, 2 = adaptive (global lipschitz), 3 = adaptive (local lipschitz))
  // For comparison with IMDP we use uniform grid
  int gridType = 1;

  // Assumptions on Kernel (Approximate (monte)  or Exact(integrals))
  // Kernel = 2 ; (monte);
  // Kernel = 1;  ( integral);
  int AssumptionKernel = 2;

  // Time horizon
  int K = 3;

  // Library (1 = simulator, 2 = faust^2, 3 = imdp)
  int lb = 2;

  // Property type
  // (1 = verify safety, 2= verify reach-avoid, 3 = safety synthesis, 4 = reach-avoid synthesis)
  int p = 1;
      
  // Task specification
//  taskSpec_t cs1SpecFAUST(lb, K, p, Safe,Input, eps, gridType, 1);
  taskSpec_t cs1SpecFAUST(lb, K, p, Safe,Target,Input, eps, gridType);
  cs1SpecFAUST.assumptionsKernel = AssumptionKernel;

  //  taskSpec_t cs1SpecFAUST(lb, K, p, Safe,Target, eps, gridType, AssumptionKernel, 0);
  //taskSpec_t cs1SpecFAUST(lb, K, p, Safe, eps, gridType, AssumptionKernel);

  // Combine model and associated task
  inputSpec_t<arma::mat, int> cs1InputFAUST(cs1SHS, cs1SpecFAUST);

  // Perform  Task
  performTask(cs1InputFAUST);

  // Perform  Task
  std::cout << " Done with with FAUST^2 "   << std::endl;
  std::cout << "----------------------------------------" << std::endl;
 /* std::cout << " Formal verification via IMDP" << std::endl;

  //  Define safe set
  arma::mat bound = {{18, 24}, {18, 24}};

  // Grid size to obtain specific state space sizes
  // --------------------------------------------------------
  // State space size| 576 | 1089   | 2304  |  3481 | 4225 
  // ________________________________________________________
  // Grid size       | 0.25 | 0.1818 | 0.125 | 0.1017| 0.09231
  // --------------------------------------------------------

  // Define grid size for 576 states
  arma::mat grid1 = {{2.25, 2.25}};

  // Relative tolerance
  arma::mat rtol1 = {{1, 1}};

  // Library (1 = simulator, 2 = faust^2, 3 = imdp)
  lb = 3;

  // Specify task to be performed
  taskSpec_t cs1Spec(lb, K, p, bound, grid1, rtol1);

  // Combine model and associated task
  inputSpec_t<arma::mat, int> cs1Input(cs1SHS, cs1Spec);

  // Perform Task
  performTask(cs1Input);
  std::cout << " Done with FAUST^2 method, now for IMDP on same grid size "
            << std::endl;
*/
  std::cout << "------------Completed Case Study 1 : Results in results "
               "folder -----------"
            << std::endl;
}
